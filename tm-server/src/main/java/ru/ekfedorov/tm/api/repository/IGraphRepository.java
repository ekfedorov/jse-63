package ru.ekfedorov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ekfedorov.tm.model.AbstractGraphEntity;

public interface IGraphRepository<E extends AbstractGraphEntity> extends JpaRepository<E, String> {
}
