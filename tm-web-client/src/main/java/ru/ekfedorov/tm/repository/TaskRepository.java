package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        create();
        create();
        create();
    }

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        Task task = new Task("task", "new");
        tasks.put(task.getId(), task);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

    public List<Task> findAll() {
        return new ArrayList<>(tasks.values()) ;
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

}

