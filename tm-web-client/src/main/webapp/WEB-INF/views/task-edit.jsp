<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="_header.jsp"/>
    <form action="/task/edit/?id=${task.id}" method="POST">
    <input type="hidden" name="id" value="${task.id}"/>
    <table class="list">
        <tr>
            <td>NAME:</td>
            <td>STATUS:</td>
            <td>DESCRIPTION:</td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" value="${task.name}"/>
            </td>
            <td>
                <select name="status">
                     <c:forEach var="st" items="${enumStatus}">
                         <c:choose>
                             <c:when test="${task.status.getDisplayName() == st.getDisplayName()}">
                                 <option selected>${st.getDisplayName()}</option>
                             </c:when>
                             <c:otherwise>
                                 <option>${st.getDisplayName()}</option>
                             </c:otherwise>
                         </c:choose>
                     </c:forEach>
                </select>
            </td>
            <td>
                <input type="text" name="description" value="${task.description}"/>
            </td>
        </tr>
    </table>
    <table class="create">
        <tr>
            <td>
                <button type="submit">SUBMIT</button>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
