package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    protected Date dateFinish;

    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}

