<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="_header.jsp"/>
    <table class="list">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>STATUS</th>
            <th>DESCRIPTION</th>
            <th class="mini">EDIT</th>
            <th class="mini">DELETE</th>
        </tr>
        <c:forEach var="task" items="${tasks}">
            <tr>
                <td>
                    <c:out value="${task.id}"/>
                </td>
                <td>
                    <c:out value="${task.name}"/>
                </td>
                <td>
                    <c:out value="${task.status.getDisplayName()}"/>
                </td>
                <td>
                    <c:out value="${task.description}"/>
                </td>
                <td class="mini">
                    <a href="/task/edit/?id=${task.id}">EDIT</a>
                </td>
                <td class="mini">
                    <a href="/task/delete/?id=${task.id}">DELETE</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <table class="create">
        <tr>
            <td>
                <form action="/task/create">
                    <button type="submit">CREATE</button>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
