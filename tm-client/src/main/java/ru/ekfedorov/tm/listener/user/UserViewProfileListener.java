package ru.ekfedorov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractUserListener;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.User;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "View my profile.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "view-profile";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@userViewProfileListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final User user = userEndpoint.findUserOneBySession(session);
        if (user == null) throw new UserIdIsEmptyException();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("USER ID: " + user.getId());
    }

}
